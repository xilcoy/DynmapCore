# NetherStuffs Block Mapping
modname:NetherStuffs

# configuration file
cfgfile:config/NetherStuffs.cfg

# Patches
patch:id=VertX1Z0ToX0Z1,Ox=1.0,Oy=0.0,Oz=0.0,Ux=0.0,Uy=0.0,Uz=1.0,Vx=1.0,Vy=1.0,Vz=0.0,visibility=flip

# Stairs
customblock:id=HellfireStair,id=AcidStair,id=DeathStair,data=*,class=org.dynmap.hdmap.renderer.StairBlockRenderer

# Saplings
patchblock:id=Saplings,data=*,patch0=VertX1Z0ToX0Z1,patch1=VertX1Z0ToX0Z1@90

# Slabs
block:id=HalfSlab_BlockId,data=0,data=1,data=2,data=3,data=4,data=5,data=6,data=7,scale=2
layer:0
**
**
layer:1
--
--
block:id=HalfSlab_BlockId,data=8,data=9,data=10,data=11,data=12,data=13,data=14,data=15,scale=2
layer:0
--
--
layer:1
**
**

# Fences and gates
# I:"Acid Fence"=1259
# I:"Acid Fence Gate"=1256
# I:"Death Fence"=1260
# I:"Death Fence Gate"=1257
# I:"Hellfire Fence"=1258
# I:"Hellfire Fence Gate"=1255
# I:"NetherBrick Fence Gate"=1254
